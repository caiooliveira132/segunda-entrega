from django.forms import ModelForm
from .models import Post, Comment, Category


class MovieForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'release_year',
            'poster_url',
            'date_post',
        ]
        labels = {
            'name': 'Título',
            'release_year': 'Texto',
            'poster_url': 'URL do Poster',
            'date_post': 'Data de Publicação'
        }

class ReviewForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
            'likes',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
            'likes': 'Data de Publicação',
        }

class CreateList(ModelForm):
    class Meta:
        model = Category
        fields = [
            'author',
            'name',
            'description',
            'movies',
        ]
        labels = {
            'author': 'Usuário',
            'name': 'Nome da Categoria',
            'description': 'Descrição',
            'movies': 'Posts',
        }