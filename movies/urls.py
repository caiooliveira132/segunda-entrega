from django.urls import path

from . import views

app_name = 'movies'
urlpatterns = [
    path('', views.ListView, name='index'),
    path('search/', views.search_movies, name='search'),
    path('create/', views.CreateView, name='create'),
    path('<int:movie_id>/', views.DetailView, name='detail'),
    path('update/<int:movie_id>/', views.UpdateView, name='update'),
    path('delete/<int:movie_id>/', views.DeleteView, name='delete'),
    path('<int:movie_id>/comment/', views.create_comment, name='comment'),
    path('lists/', views.ListListView.as_view(), name='lists'),
    path('lists/create', views.ListCreateView.as_view(), name='create-list'),
    path('lists/tecnology', views.DetailList, name='detail-list'),
]