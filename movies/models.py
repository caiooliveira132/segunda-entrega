from django.db import models
from django.conf import settings


class Post(models.Model):
    name = models.CharField(max_length=255)
    release_year = models.TextField()
    poster_url = models.URLField(max_length=200, null=True)
    date_post = models.DateTimeField()

    def __str__(self):
        return f'{self.name} ({self.release_year})'


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    likes = models.DateTimeField()
    movie = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField()
    movies = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name} by {self.author}'