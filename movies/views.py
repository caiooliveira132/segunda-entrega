from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .temp_data import movie_data
from django.shortcuts import render, get_object_or_404
from .models import Post, Comment, Category
from .forms import ReviewForm, CreateList
from django.views import generic

def DetailView(request, movie_id):
    movie = get_object_or_404(Post, pk=movie_id)
    context = {'movie': movie}
    return render(request, 'movies/detail.html', context)


def search_movies(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        movie_list = Post.objects.filter(name__icontains=search_term)
        context = {"movie_list": movie_list}
    return render(request, 'movies/search.html', context)


def CreateView(request):
    if request.method == 'POST':
        movie_name = request.POST['name']
        movie_release_year = request.POST['release_year']
        movie_poster_url = request.POST['poster_url']
        movie_date_post = request.POST['date_post']
        movie = Post(name=movie_name,
                      release_year=movie_release_year,
                      poster_url=movie_poster_url,
                      date_post=movie_date_post)
        movie.save()
        return HttpResponseRedirect(
            reverse('movies:detail', args=(movie.id, )))
    else:
        return render(request, 'movies/create.html', {})

def ListView(request):
    movie_list = Post.objects.all()
    context = {'movie_list': movie_list}
    return render(request, 'movies/index.html', context)

def UpdateView(request, movie_id):
    movie = get_object_or_404(Post, pk=movie_id)

    if request.method == "POST":
        movie.name = request.POST['name']
        movie.release_year = request.POST['release_year']
        movie_poster_url = request.POST['poster_url']
        movie.save()
        return HttpResponseRedirect(
            reverse('movies:detail', args=(movie.id, )))

    context = {'movie': movie}
    return render(request, 'movies/update.html', context)


def DeleteView(request, movie_id):
    movie = get_object_or_404(Post, pk=movie_id)

    if request.method == "POST":
        movie.delete()
        return HttpResponseRedirect(reverse('movies:index'))

    context = {'movie': movie}
    return render(request, 'movies/delete.html', context)

def create_comment(request, movie_id):
    movie = get_object_or_404(Post, pk=movie_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment_likes = form.cleaned_data['likes']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            likes=comment_likes,
                            movie=movie)
            comment.save()
            return HttpResponseRedirect(
                reverse('movies:detail', args=(movie_id, )))
    else:
        form = ReviewForm()
    context = {'form': form, 'movie': movie}
    return render(request, 'movies/review.html', context)

class ListListView(generic.ListView):
    model = Category
    template_name = 'movies/lists.html'

class ListCreateView(generic.CreateView):
    model = Category
    template_name = 'movies/create_list.html'
    fields = ['name', 'author', 'description', 'movies']
    success_url = reverse_lazy('movies:lists')

def DetailList(request):
    category = get_object_or_404(Category)
    context = {'category': category}
    return render(request, 'movies/detail_list.html', context)